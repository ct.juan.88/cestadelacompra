package org.villablanca;

import java.util.Scanner;

public class Main {

	private static int[] cantidades;
	private static String[] productos;
	private static int contador;
	private static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {

		// Arrays con un maximo de 10(Limite de la cesta)
		cantidades = new int[10];
		productos = new String[10];
		contador = 0;
		opcionMenu();
	}

	/**
	 * Este m�todo muestra por pantalla un men�.
	 * 
	 * @return Devuelve un entero como resultado de la opci�n elegida en el men�.
	 */
	public static int mostrarMenu() {
		int opcion;
		do {
			System.out.println("\n1 - A�adir producto");
			System.out.println("2 - Eliminar producto");
			System.out.println("3 - Calcular cantidad media de productos");
			System.out.println("4 - Salir del programa\n");
			opcion = teclado.nextInt();
			if (opcion < 1 || opcion > 4)
				System.out.println("\t\tOpcion seleccionada incorrecta, seleccione de nuevo");
		} while (opcion < 1 || opcion > 4);
		return opcion;
	}

	/**
	 * Este m�todo va a realizar la opci�n elegida en el men�, llamando a los
	 * correspondientes m�todos que ejecutar�n las tareas.
	 */
	public static void opcionMenu() {
		int opcion;
		String producto;
		do {
			opcion = mostrarMenu();
			switch (opcion) {
			case 1:
				producto = intProducto();
				opcionUno(producto); // Creo este m�todo para no sobrecargar del l�neas el switch.
				break;
			case 2:
				producto = intProducto();
				if (buscarProducto(producto) != -1)
					eliminarProducto(buscarProducto(producto));
				else
					System.out.println("El producto no existe en la cesta.");
				break;
			case 3:

				break;
			default:
				ordenarAlfabeticamente(productos, cantidades, contador);
				mostrarCesta();
				System.out.println("SALIENDO");
			}
		} while (opcion != 4);
	}

	/**
	 * Este m�todo se encarga de pedir el nombre del producto al usuario.
	 * 
	 * @return Devuelve un String introducido por teclado.
	 */
	public static String intProducto() {
		System.out.print("Teclee el nombre del producto: ");
		return teclado.next();
	}

	/**
	 * Este m�todo va a buscar la posici�n de un producto en el array productos.
	 * 
	 * @param producto Recibe un String con el nombre del producto a buscar.
	 * @return Devuelve un entero con la posición del producto dentro del array
	 *         productos.
	 */
	public static int buscarProducto(String producto) {
		for (int i = 0; i < contador; i++) {
			if (productos[i].equals(producto))
				return i;
		}
		return -1;
	}

	/**
	 * Este m�todo va a�adir una cantidad a un producto ya existente en el array
	 * productos.
	 * 
	 * @param cantidad Entero con la cantidad a a�adir del producto.
	 * @param posicion Entero con la posici�n del producto dentro del array.
	 */
	public static void modificarProducto(int cantidad, int posicion) {
		cantidades[posicion] += cantidad;
	}

	/**
	 * Este m�todo va a imprimir por pantalla los productos y sus cantidades, adem�s
	 * del total de productos distintos que hay en la cesta.
	 * 
	 */
	public static void mostrarCesta() {
		for (int i = 0; i < contador; i++) {
			System.out.println("Tiene " + cantidades[i] + " artículos del producto: " + productos[i]);
		}
		System.out.println("Total nº productos: " + contador);
	}

	/**
	 * Este m�todo va a a�adir un producto y una cantidad a los arrays
	 * correspondientes
	 * 
	 * @param producto Recibe un String con el nombre del producto a a�adir
	 */
	public static void addProducto(String producto) {
		productos[contador] = producto;
		System.out.print("Teclee la cantidad a añadir del producto " + productos[contador] + ": ");
		cantidades[contador] = teclado.nextInt();
		contador++;
	}

	/**
	 * 
	 * 
	 * /** Este m�todo va a reemplazar el producto que se tiene que eliminar por el
	 * �ltimo producto del array productos. Tambi�n deja vac�a la posici�n del
	 * �ltimo producto para evitar duplicados.
	 * 
	 * @param posNueva  Entero con la posici�n a eliminar y colocar el �ltimo
	 *                  producto del array.
	 * @param posUltima Entero con la posici�n del �ltimo producto.
	 */
	public static void eliminarProducto(int posNueva) {
		productos[posNueva] = productos[contador - 1];
		cantidades[posNueva] = cantidades[contador - 1];
		productos[contador - 1] = null;
		cantidades[contador - 1] = 0;
		System.out.println("\tProducto eliminado");
		contador--;
	}

	/**
	 * este m�todo va a a�adir un producto a la cesta, llamando a otros 2 m�todo
	 * que realizar�n la operaci�n.
	 * 
	 * @param producto Recibe un String con el nombre del producto a a�adir/aumentar
	 */
	public static void opcionUno(String producto) {
		if (buscarProducto(producto) == -1) {
			addProducto(producto);
		} else {
			System.out.print("Introduzca la cantidad a añadir al producto " + producto + ": ");
			int cantidad = teclado.nextInt();
			modificarProducto(cantidad, buscarProducto(producto));
		}
	}
	/** Este m�todo va a ordenar la lista de productos alfab�ticamente.
	 * 
	 * @param aux  String auxiliar para ayudar a intercambiar posiciones en el array productos.
	 * @param aux2 Entero auxiliar para ayudar a intercambiar posiciones en el array cantidades.
	 * @param posicion indica pa posicion del array dende se encuentra el primer producto por orden alfab�tico.
	 * @param primera String del producto que se encuentra en primera posicion en orden alfab�tico
	 */
	public static void ordenarAlfabeticamente(String[]productos, int[]cantidades, int contador) {
		String aux;
		int aux2, posicion;
		String primera;
		
		for(int i = 0; i <= contador; i++) {
			primera = productos[i];
			posicion = i;
			for(int j = i; j <= contador; j++) {
				if(productos[j].compareToIgnoreCase(primera) < 0) {
					primera = productos[j];
					posicion = j;
				}
			}
			aux = productos[i];
			productos[i] = primera;
			productos[posicion] = aux;
			aux2 = cantidades[i];
			cantidades[i] = cantidades[posicion];
			cantidades[posicion] = aux2;
		}
	}
}
