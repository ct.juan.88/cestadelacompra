/**
 * tarea 1: funcion mostrar menu:
1.- Añadir producto
2.- Eliminar producto
3.- Calcular media productos
4.- Salir
Devuelve una opción (del 1 a 4) Si la opción e sincorrecta, avisa y vuelve a pedir opción.

Tarea 2: Crear estructuras de datos (Arrays)
Array de enteros -> cantidades
array de String -> productos
contadorProductos -> entero

Tarea 3: crear funcion buscar producto
Recibe un String, un array de tipo String y contadorProductos
Tiene que devolver la posición dentro del array del elemento que estamos buscando
Si no existe el producto, devolver -1

Tarea 4: Añadir producto
Recibe un array de enteros, otro de producto (String), otro de cantidad y contador de productos.
Como el producto no existe en la cesta, lo añade y aumenta contadorProductos en 1.

Tarea 5: crear funcion modificarProducto
Recibe un array de enteros, otro de producto (String), otro de cantidad y contador de productos.
Agrega la cantidad de artículos de la cesta.

Tarea 6: crear funcion eliminarProducto
Recibe el array cantidades, el array productos y un String.
Como el producto esta en la cesta, busca el String en el array de productos y su cantidad y
lo elimina.
Tras borrar, cojo el último elemento y lo muevo a la posición del producto que se va a eliminar.

Tarea 7: crear función mediaProductos
Recibe el array de enteros y el contador.
Devuelve la media de las cantidades.

Tarea 8: ordenar alfabéticamente arrays

Tarea 9: función mostrarCesta
Recibe un array de enteros, otro de producto (String) y contador de productos.
Imprime los productos que hay en la cesta

Tarea 10: funcion ejecutarOpciones
switch que llame a la función correspondiente.
en caso de añadir y eliminar que previamente busque el producto y actualice el contador de 
productos como corresponda. 
 */
package org.villablanca;